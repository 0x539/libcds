#ifndef _STRING_BUILDER_H
#define _STRING_BUILDER_H

#include <stdint.h>

#define STRING_BUILDER_INITIAL_CAP 1024
#define STRING_BUILDER_GROWTH_FACTOR 2.0
#define STRING_BUILDER_REMAINING_CAP(sb) sb->cap - sb->len
#define STRING_BUILDER_END(sb) &sb->data[sb->len]
#define STRING_BUILDER_MIN_GROWTH_FACTOR(sb, mincap) ceil(log(STRING_BUILDER_GROWTH_FACTOR) / log(mincap / sb->cap))

struct string_builder_s {
  char *data;
  uint64_t cap;
  uint64_t maxcap;
  uint64_t len;
};
typedef struct string_builder_s string_builder_t;

int string_builder_init(string_builder_t *sb, uint64_t maxcap);
void string_builder_free(string_builder_t *sb);
int string_builder_grow(string_builder_t *sb, uint64_t mincap);
int string_builder_ncat(string_builder_t *sb, char *str, int64_t len);

#endif /* _STRING_BUILDER_H */

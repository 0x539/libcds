#ifndef _VECTOR_H
#define _VECTOR_H

/**
 * Don't forget to initialize this structure with vector_init() if you
 * are not using vector_new() to create the vector.
 **/
struct vector_s {
  char *data;
  size_t capacity;
  unsigned size;
};
typedef struct vector_s vector_t;

/**
 * Create a new vector. Don't forget to call vector_free() and free()
 * on the returned pointer when you stop using it.
 **/
vector_t* vector_new(size_t capacity);

/**
 * Initialize the vector when you allocate it on the stack.
 **/
int vector_init(vector_t *v, size_t capacity);

/**
 * Free the memory allocated to the vector.
 **/
void vector_free(vector_t*);

/**
 * Add data at the end of the vector.
 **/
int vector_push_back(vector_t *v, const char *data,
		     size_t length);

#endif /* _VECTOR_H */

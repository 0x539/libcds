#ifndef STACK_H
#define STACK_H

struct stack {
  unsigned long capacity;
  unsigned long length;
  void        **stack;
};

struct stack* stack_new(unsigned long initial_capacity);

int stack_init(struct stack* s, unsigned long initial_capacity);

void stack_free(struct stack *s);

int stack_push(struct stack *s, void *data);

void* stack_peek(struct stack *s);

void* stack_pop(struct stack *s);

#endif	/* STACK_H */

#include <stdlib.h>

#include "deque.h"

deque_info* deque_new () {
  deque_info *di = malloc (sizeof (deque_info));
  if (!di)
    return NULL;

  deque *dq = malloc (sizeof (deque) * 2);
  if (!dq) {
    free (di);
    return NULL;
  }

  di->head = dq;
  di->tail = dq + 1;

  di->head->next = di->tail;
  di->head->prev = NULL;
  di->tail->next = NULL;
  di->tail->prev = di->head;

  di->head->data = NULL;
  di->tail->data = NULL;

  return di;
}

void deque_free (deque_info* di) {
  if (!di)
    return;

  deque *cur, *next;
  for (cur = di->head->next; cur != di->tail; cur = next) {
    next = cur->next;
    free(cur);
  }

  free (di->head);
}

deque* deque_push_back (deque_info *di, void* data) {
  deque *dq = malloc (sizeof (deque));
  if (!dq) {
    return NULL;
  }

  dq->prev = di->tail->prev;
  di->tail->prev->next = dq;
  di->tail->prev = dq;
  dq->next = di->tail;
  dq->data = data;

  return dq;
}

deque* deque_pop_front (deque_info *di) {
  deque *dq = di->head->next;
  if (dq == di->tail) {
    return NULL;
  }

  dq->next->prev = di->head;
  di->head->next = dq->next;

  return dq;
}

deque* deque_pop_back (deque_info *di) {
  deque *dq = di->tail->prev;
  if (dq == di->head) {
    return NULL;
  }

  dq->prev->next = di->tail;
  di->tail->prev = dq->prev;

  return dq;
}

void deque_delete(deque *d) {
  if (!d)
    return;

  if (d->next)
    d->next->prev = d->prev;

  if (d->prev)
    d->prev->next = d->next;
}

int deque_is_empty_p(deque_info* di) {
  int empty = 0;
  if (di->head->next == di->tail)
    empty = 1;

  return empty;
}

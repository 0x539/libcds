#ifndef _VECTOR_PRIVATE_H
#define _VECTOR_PRIVATE_H

#define SIZE_MULTIPLICATOR 1.5

int vector_increase_size(vector_t* v, size_t min_size);

#endif	/* _VECTOR_PRIVATE_H */

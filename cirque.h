#ifndef _CIRQUE_H
#define _CIRQUE_H

#include <stdlib.h>

struct cirque {
  void *start, *end, *data_start, *data_end;
  size_t capacity, size;
};

struct cirque* cirque_new          (size_t capacity);
int            cirque_init         (struct cirque *queue, size_t capacity);
void           cirque_free         (struct cirque *queue);
void           cirque_insert       (struct cirque *queue, const void *data, size_t length);
void           cirque_remove       (struct cirque *queue, size_t length);
int cirque_full_p(struct cirque*);

#endif /* _CIRQUE_H */

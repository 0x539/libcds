#ifndef _DEQUE_H
#define _DEQUE_H

typedef struct deque {
  struct deque *prev, *next;
  void *data;
} deque;

typedef struct deque_info {
  deque *head, *tail;
} deque_info;

/* deque functions */
deque_info* deque_new       ();
void        deque_free      (deque_info*);
deque*      deque_push_back (deque_info*, void* data); 
deque*      deque_pop_front (deque_info*);
deque*      deque_pop_back  (deque_info*);
void        deque_delete    (deque*);
int         deque_is_empty_p(deque_info*);

#endif /* _DEQUE_H */

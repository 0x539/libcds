#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "string_builder.h"

int string_builder_init(string_builder_t *sb, uint64_t maxcap)
{
  uint64_t initial_capacity = STRING_BUILDER_INITIAL_CAP;
  if (STRING_BUILDER_INITIAL_CAP > maxcap) {
	initial_capacity = maxcap;
  }

  sb->maxcap = maxcap;
  sb->cap = initial_capacity;
  sb->len = 0;

  sb->data = malloc(sizeof(char) * initial_capacity);
  if (!sb->data) {
	return -1;
  }

  return 0;
}

void string_builder_free(string_builder_t *sb)
{
  free(sb->data);
}

int string_builder_grow(string_builder_t *sb, uint64_t mincap)
{
  if (sb->maxcap == sb->cap || sb->maxcap <= mincap) {
	return -1;
  }

  uint64_t newcap = ceil(sb->cap * STRING_BUILDER_MIN_GROWTH_FACTOR(sb, mincap));
  if (newcap > sb->maxcap) {
	newcap = sb->maxcap;
  }

  void *mem = realloc(sb->data, newcap);
  if (!mem) {
	return -1;
  }

  sb->data = mem;

  return 0;
}

int string_builder_ncat(string_builder_t *sb, char *str, int64_t len)
{
  if (!sb || !str)
	return -1;

  if (len == -1)
	len = strlen(str);

  if (STRING_BUILDER_REMAINING_CAP(sb) < len) {
	if (string_builder_grow(sb, sb->len + len)) {
	  return -1;
	}
  }

  memcpy(STRING_BUILDER_END(sb), str, len);
  sb->len += len;

  return 0;
}

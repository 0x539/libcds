#include <stdlib.h>
#include <string.h>

#include "cirque.h"

struct cirque* cirque_new(size_t capacity) {
  struct cirque *queue = malloc(sizeof (struct cirque));
  if (!queue) {
    return NULL;
  }

  int ret = cirque_init(queue, capacity);
  if (ret) {
    free(queue);
    return NULL;
  }

  return queue;
}

int cirque_init(struct cirque *queue, size_t capacity) {
  if (capacity == 0) {
    return -1;
  }

  queue->start = malloc(capacity);
  if (!queue->start) {
    return -1;
  }

  queue->end = queue->start + capacity - 1;
  queue->data_start = queue->start;
  queue->data_end = queue->start;
  queue->capacity = capacity;
  queue->size = 0;

  return 0;
}

void cirque_free(struct cirque *queue) {
  free(queue->start);
}

void cirque_insert(struct cirque *queue, const void *data, size_t length) {
  if (queue->data_end + length > queue->end) {
    unsigned first_part_size = queue->end - queue->data_end + 1;
    memcpy(queue->data_end, data, first_part_size);
    memcpy(queue->start, data + first_part_size, length - first_part_size);
    queue->data_end = queue->start + length - first_part_size;
  } else {
    memcpy(queue->data_end, data, length);
    queue->data_end += length;
  }

  queue->size += length;
}

void cirque_remove(struct cirque *queue, size_t length) {
  int pos = (queue->data_start - queue->start + length) % queue->capacity;
  queue->data_start = queue->start + pos;
  queue->size -= length;
}


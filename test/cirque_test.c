#include <string.h>
#include <cirque.h>
#include "test.c"

void test_cirque_new   (void);
void test_cirque_init  (void);
void test_cirque_insert(void);
void test_cirque_remove(void);

typedef struct cirque cirque;

int
main(int argc, char *argv[]) {
  test_cirque_new   ();
  test_cirque_init  ();
  test_cirque_insert();
  test_cirque_remove();

  return 0;
}

void test_cirque_new(void) {
  printf("test_cirque_new()\n");
  size_t cap = 10;
  cirque *q = cirque_new(cap);
  if (!q) {
    fail("cirque_new returned NULL\n");
  } else if (q->size != 0) {
    fail("q->size (%d) != 0\n", q->size);
  } else if (q->capacity != cap) {
    fail("q->capacity (%d) != cap (%d)", q->capacity, cap);
  }
  cirque_free(q);
  free(q);

  cap = 200;
  q = cirque_new(cap);
  if (!q) {
    fail("cirque_new returned NULL\n");
  } else if (q->size != 0) {
    fail("q->size (%d) != 0\n", q->size);
  } else if (q->capacity != cap) {
    fail("q->capacity (%d) != cap (%d)", q->capacity, cap);
  }
  cirque_free(q);

  cap = 0;
  q = cirque_new(cap);
  if (q) {
    fail("cirque_new (%d) should return NULL\n", cap);
    cirque_free(q);
  }

  cap = -1;
  q = cirque_new(cap);
  if (q) {
    fail("cirque_new (%d) should return NULL\n", cap);
    cirque_free(q);
  }
}

void test_cirque_init(void) {
  printf("test_cirque_init()\n");
  cirque q;
  int cap = 33;
  int ret = cirque_init(&q, cap);
  if (ret) {
    fail("cirque_init() failed\n");
  } else if (q.capacity != cap) {
    fail("q.capacity (%d) != %d\n", q.capacity, cap);
  } else if (q.size != 0) {
    fail("q.size (%d) != 0\n", q.size);
  }
  cirque_free(&q);

  cap = 0;
  ret = cirque_init(&q, cap);
  if (!ret) {
    fail ("cirque_init(%d) should have failed.\n", cap);
  }
}

void test_cirque_insert(void) {
  printf ("test_cirque_insert()\n");
  cirque *q = cirque_new(100);

  const char *test_string1 = "abc";
  cirque_insert(q, test_string1, strlen(test_string1));
  if (q->size != strlen(test_string1)) {
    fail("cirque_insert() failed\n");
  }

  const char *test_string2 = "de";
  cirque_insert(q, test_string2, strlen(test_string2));
  if (q->size != strlen(test_string1) + strlen(test_string2)) {
    fail("cirque_insert() failed\n");
  }

  if (strncmp (q->start, "abcde", 5)) {
    fail ("error: cirque_insert() pushed random data\n");
  }

  cirque_free(q);
  free(q);
}

void test_cirque_remove(void) {
  printf("test_cirque_remove()\n");
  cirque *q = cirque_new(100);

  const char *test_string1 = "abc";
  cirque_insert(q, test_string1, strlen(test_string1));
  if (q->size != strlen(test_string1)) {
    fail ("cirque_insert() failed\n");
  }

  const char *test_string2 = "de";
  cirque_insert (q, test_string2, strlen(test_string2));
  if (q->size != strlen(test_string1) + strlen(test_string2)) {
    fail ("cirque_insert() failed\n");
  }

  cirque_remove (q, 3);
  if (q->size != 2) {
    fail ("cirque_remove() failed\n");
  }

  cirque_remove (q, 2);
  if (q->size != 0) {
    fail ("cirque_remove() failed\n");
  }

  cirque_free(q);
}

#include <stdlib.h>
#include <string.h>

#include "vector.h"
#include "vector_private.h"

vector_t* vector_new(size_t capacity) {
  if (capacity <= 0)
    return NULL;

  vector_t *v = malloc(sizeof (vector_t));
  if (!v)
    return v;

  int ret = vector_init(v, capacity);
  if (ret) {
    free(v);
    return NULL;
  }

  return v;
}

int vector_init(vector_t *v, size_t capacity) {
  if (!v || capacity == 0) {
    return -1;
  }

  v->data = malloc(sizeof (char) * capacity);
  if (!v->data) {
    return -1;
  }

  v->capacity = capacity;
  v->size = 0;

  return 0;
}

void vector_free (vector_t *v) {
  if (v->capacity > 0)
    free (v->data);
}

int vector_push_back (vector_t *v, const char *data, size_t length) {
  if (length <= 0) {
    return -1;
  }

  int ret;
  if (v->size + length > v->capacity) {
    ret = vector_increase_size(v, v->size + length);
    if (ret) {
      return ret;
    }
  }

  memcpy(v->data + v->size, data, length);
  v->size += length;

  return 0;
}

int vector_increase_size(vector_t* v, size_t min_size) {
  size_t new_size = ((double) v->capacity) * SIZE_MULTIPLICATOR;
  if (min_size > new_size) {
    new_size = ((double) min_size) * SIZE_MULTIPLICATOR;
  }

  void *new_mem = realloc(v->data, new_size);
  if(!new_mem) {
    return -1;
  }

  v->data = new_mem;
  v->capacity = new_size;

  return 0;
}

#include <stdlib.h>
#include "stack.h"

struct stack* stack_new (unsigned long initial_capacity) {
  struct stack *s = malloc(sizeof (struct stack));
  if (!s)
    return NULL;

  if (stack_init(s, initial_capacity)) {
    free(s);
    return NULL;
  }

  return s;
}

int stack_init (struct stack* s, unsigned long initial_capacity) {
  if (initial_capacity == 0)
    return -1;

  s->stack = malloc (sizeof (void*) * initial_capacity);
  if (!(s->stack))
    return -1;

  s->capacity = initial_capacity;
  s->length = 0;

  return 0;
}

void
stack_free (struct stack *s)
{
  free (s->stack);
}

static int
stack_increase_capacity (struct stack *s, unsigned long nb)
{
  void *new_stack = realloc (s->stack, sizeof (void*) * s->capacity + nb);
  if (!new_stack)
    return -1;

  s->stack = new_stack;
  s->capacity += nb;

  return 0;
}

int
stack_push (struct stack *s, void *data)
{
  if (!s || !data)
    return -1;

  if (s->length + 1 == s->capacity)
    {
      /* Double stack capacity */
      int ret = stack_increase_capacity (s, s->capacity);
      if (ret)
        return ret;
    }

  s->stack[s->length] = data;
  s->length += 1;

  return 0;
}

void*
stack_peek (struct stack *s)
{
  if (s->length <= 0)
    return NULL;
  return s->stack[s->length - 1];
}

void*
stack_pop (struct stack *s)
{
  if (s->length == 0)
    return NULL;

  --(s->length);

  return s->stack[s->length];
}

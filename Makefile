.POSIX:
.SUFFIXES: .o .c

PREFIX=/usr/local

CFLAGS=-fPIC -std=c99 -Wall

TARGET=libcds
SRC=vector.c deque.c stack.c cirque.c string_builder.c
OBJ=${SRC:.c=.o}
HEADERS=${SRC:.c=.h}

all: libcds.a libcds.so

$(TARGET).a: ${OBJ}
	$(AR) $(ARFLAGS) $@ ${OBJ}
	ranlib $@

$(TARGET).so: ${OBJ}
	gcc -shared -o $@ ${OBJ}

clean:
	rm -f *.o $(TARGET).{so,a}
	$(MAKE) -C test clean

check: libcds.a
	$(MAKE) -C test

install: libcds.a libcds.so
	install -d $(DESTDIR)$(PREFIX)/lib/ $(DESTDIR)$(PREFIX)/include/
	install -m 644 $(TARGET).{a,so} $(DESTDIR)$(PREFIX)/lib/
	install -m 644 ${HEADERS} $(DESTDIR)$(PREFIX)/include/

.PHONY: all clean check install

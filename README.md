# How to compile

`$ make`

# How to install

To install in /usr/local (the default) type:

`# make install`

To install to another directory, e.g. /usr type:

`# make PREFIX=/usr install`

To put the installed files in another directory, for example to create
a binary archive of the library, type:

`$ make DESTDIR=/path/to/directory install`

And you can combine `DESTDIR` with `PREFIX`:

`$ make PREFIX=/usr DESTDIR=/path/to/directory install`

# Run test suite

`$ make check`
